#include <Arduino.h>
#include <U8g2lib.h>

#include "logo.h"
#include "Receiver.h"

#define LED_PIN 13

#define OLED_SCL_PIN 11
#define OLED_SDA_PIN 10

#define VBAT_SENSE_PIN A0
#define VBAT_SENSE_VREF 4.35f
#define VBAT_SENSE_MUL ((1.0f / 1024.0f * VBAT_SENSE_VREF) * 11.0f)
#define VBAT_SENSE_AVG_READS 10

#define RECEIVER_COUNT 6
#define RECEIVER_SPI_CLK_PIN 2
#define RECEIVER_SPI_DATA_PIN 3
#define RECEIVER_SPI_SS_START_PIN 4

#define BUTTON_UP_PIN A1
#define BUTTON_OK_PIN A2
#define BUTTON_DOWN_PIN A3

#define BOOT_DELAY 2500


U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(
  U8G2_R0,
  OLED_SCL_PIN,
  OLED_SDA_PIN,
  U8X8_PIN_NONE);

Receiver receivers[RECEIVER_COUNT];


void setup(void) {
  pinMode(OLED_SCL_PIN, OUTPUT);
  pinMode(OLED_SDA_PIN, OUTPUT);
  digitalWrite(OLED_SCL_PIN, LOW);
  digitalWrite(OLED_SDA_PIN, LOW);

  pinMode(RECEIVER_SPI_CLK_PIN, OUTPUT);
  pinMode(RECEIVER_SPI_DATA_PIN, OUTPUT);
  digitalWrite(RECEIVER_SPI_CLK_PIN, LOW);
  digitalWrite(RECEIVER_SPI_DATA_PIN, LOW);
  for (int i = 0; i < RECEIVER_COUNT; i++) {
    pinMode(RECEIVER_SPI_SS_START_PIN + i, OUTPUT);
    digitalWrite(RECEIVER_SPI_SS_START_PIN + i, HIGH);
  }

  pinMode(VBAT_SENSE_PIN, INPUT);

  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_OK_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);

  u8g2.begin();

  drawLogoScreen();
  delay(BOOT_DELAY);

  for (int i = 0; i < RECEIVER_COUNT; i++) {
      receivers[i].init(
          RECEIVER_SPI_CLK_PIN,
          RECEIVER_SPI_DATA_PIN,
          RECEIVER_SPI_SS_START_PIN + i
      );
  }

  receivers[0].setFrequency(5658);
  receivers[1].setFrequency(5695);
  receivers[2].setFrequency(5760);
  receivers[3].setFrequency(5800);
  receivers[4].setFrequency(5880);
  receivers[5].setFrequency(5917);
}


void loop(void) {
  drawVbatSenseScreen();

  bool shouldLight = \
    digitalRead(BUTTON_UP_PIN) == LOW \
    || digitalRead(BUTTON_OK_PIN) == LOW \
    || digitalRead(BUTTON_DOWN_PIN) == LOW;

  digitalWrite(LED_PIN, shouldLight);
}


float getVbatSense() {
  int avgCumulative = 0;
  for (int i = 0; i < VBAT_SENSE_AVG_READS; i++) {
    avgCumulative += analogRead(VBAT_SENSE_PIN);
  }

  float raw = (float) avgCumulative / VBAT_SENSE_AVG_READS;
  float val = raw * VBAT_SENSE_MUL;

  return val;
}


void drawVbatSenseScreen() {
  u8g2.clearBuffer();

  float vbatSenseVal = getVbatSense();
  String vbatSenseString = String(vbatSenseVal, 1) + 'V';
  const char* vbatSenseText = vbatSenseString.c_str();

  u8g2.setFont(u8g2_font_profont22_mr);
  u8g2.setFontRefHeightAll();
  u8g2.setFontPosCenter();
  int strWidth = u8g2.getStrWidth(vbatSenseText);
  int xPos = (u8g2.getDisplayWidth() / 2) - (strWidth / 2);
  int yPos = u8g2.getDisplayHeight() / 2;
  u8g2.drawStr(xPos, yPos, vbatSenseText);

  drawFrame();
  u8g2.sendBuffer();
}


void drawLogoScreen() {
  u8g2.clearBuffer();
  u8g2.drawXBMP(0, 0, LOGO_WIDTH, LOGO_HEIGHT, logo_bitmap);
  drawFrame();
  u8g2.sendBuffer();
}


void drawFrame() {
  u8g2.drawFrame(0, 0, u8g2.getDisplayWidth(), u8g2.getDisplayHeight());
}
