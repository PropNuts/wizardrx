# WizardRX

This repository contains the hardware and firmware for the WizardRX, a low-cost
6 way 5.8GHz video receiver. It can be hand-soldered easily using cheap
parts found online, costing roughly £60-70.

![](images/pcb_v1_diy.png)

Channels can be set through a three-button interface, with an OLED display for
feedback. There is also voltage monitoring to mkae

## Motiviation

We wanted to be able to view video feeds at our race events. Buying six
standalone receivers cost too much (roughly £120 or more, double that if you
want diversity) and take up too much space. This seemed like a better way
forward.


## Status

The current working revision is the v2.0 board available in this repository.
Previous boards didn't quite work out as expected!

The firmware is working but currently has no UI for setting channels, but can
display battery voltage. Channels are set in code when the firmware is uploaded.


# Hardware

The KiCad and Gerber files can be found under `hardware/`. The PCB is dual-layer
with no particular requirements.

[JLCPCB](https://jlcpcb.com/) was used for fabrication and so the board is
designed against their list of [capabilities](https://jlcpcb.com/capabilities/Capabilities).
If you decided to use another PCB fabricator please make sure their capabilities
either surpass or meet these ones.

Check out the [tags](https://gitlab.com/PropNuts/wizardrx/tags) for pre-packaged
Gerber files.

## Parts List

- [(6x) RX5808 modules (with SPI resistor mod)](https://www.banggood.com/FPV-Wireless-Audio-Video-Receiving-Module-RX5808-Receiver-p-84775.html?p=U103211727939201506N)
- [(1x) I2C OLED screen](https://www.banggood.com/0_96-Inch-4Pin-White-IIC-I2C-OLED-Display-Module-12864-LED-For-Arduino-p-958196.html?p=U103211727939201506N)
- [(3x) 6x6mm Microswitches](https://www.banggood.com/100pcs-Mini-Micro-Momentary-Tactile-Tact-Switch-Push-Button-DIP-P4-Normally-Open-p-917570.html?p=U103211727939201506N)
- [(6x) SMA (or RP-SMA) pigtail extensions (as short as possible)](https://www.banggood.com/DIY-150mm-SMARP-SMA-Female-to-PCB-Solder-Pigtail-Antenna-Extension-Adapter-Cable-p-1224246.html?p=U103211727939201506N)
- [(1x) Arduino Nano](https://www.banggood.com/ATmega328P-Nano-V3-Controller-Board-Compatible-Arduino-p-940937.html?p=U103211727939201506N)
- [(1x) 5V regulator](https://www.banggood.com/3pcs-DC-DC-5V-3A-Power-Supply-Module-Buck-Step-Down-Regulator-Module-24V-12V-9V-To-5V-Fixed-Output-p-1198420.html?p=U103211727939201506N)
- [(1x) 3.3V regulator](https://www.banggood.com/3pcs-DC-DC-3_3V-3A-Power-Supply-Module-Buck-Regulator-Module-12V-5V-To-3_3V-Fixed-Output-Car-Power-p-1209469.html?p=U103211727939201506N)
- [(1x) 100k resistor and (1x) 10k resistor](https://www.banggood.com/Wholesale-600pcs-30-Kinds-Value-Metal-Film-Resistor-Assorted-Kit-20pcs-Each-Value-p-53320.html?p=U103211727939201506N)
- [(1x) 1N5817 Schottky diode (or similiar)](https://www.banggood.com/100-Pcs-030892-Diodes-Package-Rectifier-Schottky-8-Type-In-Component-Box-p-1164787.html?p=U103211727939201506N)
- [(1x) Female XT60 connector](https://www.banggood.com/20Pcs-XT60-500V-30A-Male-Female-Bullet-Connectors-Plug-Sockets-p-1257187.html?p=U103211727939201506N)
- (6x) Female RCA jacks (check your local eBay)

The above links are affiliate links where possible. Please support this project
by making use of them.


## Ordering

Hit up [JLCPCB's quote page](https://jlcpcb.com/quote) and upload the Gerber
files along with how many you want, what colour, etc. The default settings are
absolutely fine.

![](images/jlcpcb_ordering.png)

You are on your own if you use another fab but if you go for these sort of
settings, it should work out OK.


## Building

More info to come. Use common sense, everything goes where you think it goes.
tl;dr:

1. Slap it all together with your soldering iron, a lot of wire, and some
   patience.
2. Upload the firmware to the Arduino.
3. Pray the magic smoke stays in.


## Case

A 3D printed case is available.

![](images/case.jpg)

Check it out on [Thingiverse](https://www.thingiverse.com/thing:3122498) for more details.


# Firmware

The firmware is Arduino-based can be found under `firmware/`. Arduino 1.8.x or
later and U8g2 is required.


# Changelog

__v2.0 (2018/09/27)__
  - Initial release.


# License

The project is licensed under the terms of the MIT license.
